package ru.tsc.karbainova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.listener.JpaEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@EntityListeners(JpaEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserDTO extends AbstractDTOEntity {

    @Column
    @NonNull
    private String login;
    @NonNull
    @Column(name = "password_hash")
    private String passwordHash;
    @Column
    @Nullable
    private String email;
    @Nullable
    @Column(name = "first_name")
    private String firstName;
    @Nullable
    @Column(name = "last_name")
    private String lastName;
    @Nullable
    @Column(name = "middle_name")
    private String middleName;
    @Column
    @NonNull
    private Role role = Role.USER;
    @Column
    private Boolean locked = false;

}
