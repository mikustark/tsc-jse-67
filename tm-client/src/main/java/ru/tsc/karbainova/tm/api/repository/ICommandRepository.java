package ru.tsc.karbainova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.karbainova.tm.listener.AbstractListener;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;

import java.util.Collection;
import java.util.Map;

public interface ICommandRepository {

    AbstractListener getCommandByName(String name);

    AbstractListener getCommandByArg(String arg);

    Map<String, AbstractListener> getCommands();

    Map<String, AbstractSystemListener> getArguments();

    Collection<String> getCommandArg();

    Collection<String> getCommandName();

    void create(AbstractSystemListener command);

    void add(@NotNull AbstractListener command);
}
