package ru.tsc.karbainova.tm.component;

import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.api.service.ILogService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.api.service.dto.IAdminUserService;
import ru.tsc.karbainova.tm.api.service.dto.IProjectService;
import ru.tsc.karbainova.tm.api.service.dto.IProjectToTaskService;
import ru.tsc.karbainova.tm.api.service.dto.ISessionService;
import ru.tsc.karbainova.tm.api.service.dto.ITaskService;
import ru.tsc.karbainova.tm.api.service.dto.IUserService;
import ru.tsc.karbainova.tm.endpoint.AbstractEndpoint;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Component
public class Bootstrap implements ServiceLocator {

    @NotNull
    @Autowired
    private IPropertyService propertyService;
    @NotNull
    @Autowired
    private IAdminUserService adminUserService;
    @NotNull
    @Autowired
    private IUserService userService;
    @NotNull
    @Autowired
    private IProjectService projectService;
    @NotNull
    @Autowired
    private ITaskService taskService;
    @NotNull
    @Autowired
    private IProjectToTaskService projectToTaskService;
    @NotNull
    @Autowired
    private ILogService logService;
    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    private void initEndpoint(final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initEndpoint() {
        Arrays.stream(endpoints).forEach(this::initEndpoint);
    }

    public void init() throws Exception {
        displayWelcome();
        initPID();
        initEndpoint();
        initJMS();
//        adminUserService.create("admin", "admin", Role.ADMIN);
    }

    @SneakyThrows
    public void initJMS() {
        @NotNull final BrokerService broker = new BrokerService();
        @NotNull final String bindAddress = "tcp://" + BrokerService.DEFAULT_BROKER_NAME + ":" + BrokerService.DEFAULT_PORT;
        broker.addConnector(bindAddress);
        broker.start();
    }

    @SneakyThrows
    private void initPID() {
        @NonNull final String filename = "task-manager.pid";
        @NonNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NonNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private static long getPID() {
        final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            } catch (@NonNull Exception e) {
                return 0;
            }
        }
        return 0;
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

}
