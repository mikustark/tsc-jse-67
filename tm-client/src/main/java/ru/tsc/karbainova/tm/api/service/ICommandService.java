package ru.tsc.karbainova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.listener.AbstractListener;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;

import java.util.Collection;
import java.util.Map;

public interface ICommandService {

    AbstractListener getCommandByName(String name);

    AbstractListener getCommandByArg(String arg);

    Map<String, AbstractListener> getCommands();

    Map<String, AbstractSystemListener> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListCommandArg();

    void create(AbstractSystemListener command);

    void add(@Nullable AbstractListener command);

}
