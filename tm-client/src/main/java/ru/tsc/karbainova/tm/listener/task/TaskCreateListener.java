package ru.tsc.karbainova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractListenerTask;
import ru.tsc.karbainova.tm.listener.TerminalUtil;

@Component
public class TaskCreateListener extends AbstractListenerTask {
    @Override
    public String name() {
        return "create-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create task";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        taskEndpoint.createTaskAllParam(sessionService.getSession(), name, description);
        System.out.println("[OK]");
    }

}
