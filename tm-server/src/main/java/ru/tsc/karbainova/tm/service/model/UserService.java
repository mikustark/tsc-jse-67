package ru.tsc.karbainova.tm.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.repository.model.UserRepository;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.model.IUserServiceModel;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.empty.EmptyEmailException;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyLoginException;
import ru.tsc.karbainova.tm.exception.empty.EmptyPasswordException;
import ru.tsc.karbainova.tm.exception.empty.EmptyUserNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.service.PropertyService;
import ru.tsc.karbainova.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

@Service
public class UserService extends AbstractService<User> implements IUserServiceModel {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    public boolean isLoginExists(@NonNull final String login) {
        if (login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    @SneakyThrows
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    @SneakyThrows
    public void addAll(Collection<User> collection) {
        if (collection == null) return;
        for (User i : collection) {
            add(i);
        }
    }

    @Override
    @SneakyThrows
    public User findByLogin(@NonNull final String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    @SneakyThrows
    public User findById(@NonNull final String id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    @SneakyThrows
    public User create(@NonNull final String login, @NonNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        userRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    @SneakyThrows
    public User create(@NonNull final String login, @NonNull final String password, @NonNull final String email) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new EmptyLoginException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        userRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    @SneakyThrows
    public User setPassword(@NonNull final String userId, @NonNull final String password) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        IPropertyService propertyService = new PropertyService();
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if (user == null) return null;
        final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        return user;
    }


    @Override
    @Transactional
    @SneakyThrows
    public User updateUser(
            @NonNull final String userId,
            @NonNull final String firstName,
            @NonNull final String lastName,
            @Nullable final String middleName) {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) throw new ProjectNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    @SneakyThrows
    public User add(User user) {
        if (user == null) throw new EmptyUserNotFoundException();
        userRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    @SneakyThrows
    public void clear() {
        userRepository.clear();
    }
}
