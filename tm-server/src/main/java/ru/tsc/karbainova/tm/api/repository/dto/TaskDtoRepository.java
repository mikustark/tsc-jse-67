package ru.tsc.karbainova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import java.util.List;

public interface TaskDtoRepository extends JpaRepository<TaskDTO, String> {

    void deleteByUserId(@NotNull String userId);

    void deleteByProjectId(@NotNull String projectId);

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId);

    @Nullable
    TaskDTO findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    TaskDTO findFirstByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    long countByUserId(@NotNull String userId);

    @Modifying
    @Query("UPDATE TaskDTO e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :id")
    void bindTaskById(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId,
            @Param("id") @NotNull String taskId
    );

    @Modifying
    @Query("UPDATE TaskDTO e SET e.projectId = NULL WHERE e.userId = :userId AND e.projectId = :projectId AND e.id = :id")
    void unbindTaskById(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId,
            @Param("id") @NotNull String taskId
    );

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

}
