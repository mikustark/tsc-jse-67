package ru.tsc.karbainova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tsc.karbainova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/projects")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(projectService.findAll());
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Project create(@RequestBody final Project project) {
        projectService.save(project);
        return project;
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public Project save(@RequestBody final Project project) {
        projectService.save(project);
        return project;
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        projectService.removeById(id);
    }
}
