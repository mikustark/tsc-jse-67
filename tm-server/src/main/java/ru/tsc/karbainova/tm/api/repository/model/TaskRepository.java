package ru.tsc.karbainova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.User;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, String> {
    void clear();

    void clearByUserId(@NotNull String userId);

    Task findByName(@Nullable String userId, @Nullable String name);

    @NotNull List<Task> findAllByUserId(@Nullable String userId);

    @Nullable Task findByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable Task findByIndex(@Nullable String userId, @NotNull Integer index);

    void removeByName(@Nullable String userId, @Nullable String name);

    void removeByIdUserId(@Nullable String userId, @NotNull String id);

    @Modifying
    @Query("UPDATE Task e SET e.projectId = :projectId WHERE e.user = :user AND e.id = :id")
    void bindTaskById(
            @Param("user") @NotNull User user,
            @Param("projectId") @NotNull String projectId,
            @Param("id") @NotNull String taskId
    );

    @Modifying
    @Query("UPDATE Task e SET e.projectId = NULL WHERE e.user = :user AND e.projectId = :projectId AND e.id = :id")
    void unbindTaskById(
            @Param("user") @NotNull User user,
            @Param("projectId") @NotNull String projectId,
            @Param("id") @NotNull String taskId
    );

    @NotNull List<Task> findTasksByUserIdProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    void removeTasksByProjectId(@Nullable String projectId);
}
